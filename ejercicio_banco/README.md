Consideraciones:

	Respecto de la logica de negocio

		1. Un usuario solo puede loguearse si previamente ya estaba registrado
		2. No puede haber dos cliente con el mismo mail
		3. Para registrarse en homeBanking un usuario previamente debe tener un codigo generado por el banco
		4. Un password debe contener al menos 1 caracter en mayuscula
		5. Un password debe tener una cantidad minima de caracteres que estara determinada por el banco
		6. El home banking maneja la cantidad de maxima posible de ingreso fallido de password. Es posible consultar y modificar dicha cantidad maxima.

	Respecto del diseño
		1. El banco es el encargado de administrar a lo clientes, es decir, que no exista mas de dos clientes con el mismo mail y que las reglas de negocio para las contraseñas se cumplan.

Observacion de entrega:

	Debido a problemas a la hora de generar el branch:
		1. Se elimino el repositorio original donde se habia realizado la solucion a este ejercicio.
		2. Se creo un proyecto nuevo llamado "IngenieriaDeSoftware"
		3. Se genero un nuevo branch donde se copiaron todas las carpetas que representan la solucion del ejercicio. Por esta razon en la historia de este branch solo aparece 1 solo commit 

