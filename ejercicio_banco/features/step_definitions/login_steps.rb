Given(/^my user is "([^"]*)" and password "([^"]*)"$/) do |mail, password|
  @email = mail
  @password_usuario = password

  banco = Banco.new  
  cliente = banco.agregar_cliente(@email, @password_usuario)
  @home_banking = HomeBanking.new(banco)
  @home_banking.registrar_usuario(@email, @password_usuario, banco.generar_codigo_home_banking(cliente))
end

When(/^I login with "([^"]*)" and password "([^"]*)"$/) do |mail, password|
  @acceso = @home_banking.loguear_usuario(mail, password)
end

Then(/^I access the system$/) do  
   expect(@acceso).to eq true
end

Then(/^I do not access the system$/) do
   expect(@acceso).to eq false
end
