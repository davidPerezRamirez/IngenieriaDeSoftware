Given(/^the ATM generated code "([^"]*)"$/) do |code|  
  @codigo_home_banking = code.to_i
end

When(/^I register with email "([^"]*)", password "([^"]*)" and code "([^"]*)"$/) do |mail, password, code|  
  banco = Banco.new  
  banco.agregar_cliente(mail, password, @codigo_home_banking)
  home_banking = HomeBanking.new(banco)  
  @usuario = home_banking.registrar_usuario(mail, password, code.to_i);
end

Then(/^my account is created$/) do  
  expect(@usuario).not_to eq nil
end

Then(/^my account is not created$/) do
  expect(@usuario).to eq nil
end
