Given(/^I have already registered$/) do
  banco = Banco.new  
  @cliente = banco.agregar_cliente("email@gmail.com", "R1234567")
  @cliente.codigo_home_banking = banco.generar_codigo_home_banking(@cliente);
  @home_banking = HomeBanking.new(banco)
  @home_banking.registrar_usuario("email@gmail.com", "R123", @cliente.codigo_home_banking)  
end

When(/^I attemp (\d+) failed logins$/) do |cant_max_fallos|
	@fallos = cant_max_fallos.to_i
  (0..@fallos).each { |intento|  
    @home_banking.loguear_usuario("email@gmail.com", "R1hgng54b1zfd4814")
  }	
end

Then(/^my credentials get blocked$/) do  
  expect(@cliente.tiene_credenciales_bloquedas?(@fallos)).to eq true
end