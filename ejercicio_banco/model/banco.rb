require_relative './repositorioDeUsuarios'
require_relative './clienteBanco'
require_relative './validadorPassword'
require_relative './generadorDeCodigoHomeBanking'

class Banco

	def initialize 
		@repositorio_usuarios = RepositorioDeUsuarios.new
		@generadorDeCodigo = GeneradorDeCodigoHomeBanking.new
		@validadorPassword= ValidadorPassword.new
	end

	def agregar_cliente(email, password, code_home_banking = nil)	
		if (@validadorPassword.es_password_valido?(password) && (!existe_cliente?(email))) then
			cliente_banco = ClienteBanco.new(email, password, code_home_banking)
			@repositorio_usuarios.agregar_usuario(cliente_banco)
		end
		
		return cliente_banco
	end

	def existe_cliente? (email)
		@repositorio_usuarios.existe_usuario?(email)
	end
	def generar_codigo_home_banking(cliente)
		@generadorDeCodigo.generar_codigo_home_banking(cliente)
	end

	def cantidad_usuarios_registrados
		@repositorio_usuarios.cantidad_usuarios_registrados
	end

	def obtener_cliente(email) 
		@repositorio_usuarios.obtener_usuario(email)
	end

end