class ClienteBanco	

	attr_accessor :codigo_home_banking
	attr_reader :email

	def initialize(email, password, codigo = nil)		
		@email = email		
		@password = password
		@codigo_home_banking = codigo
		@intentos_logueo_fallidos = 0
	end

	def tiene_credenciales_bloquedas?(maximo_intentos_fallos)
		@intentos_logueo_fallidos >= maximo_intentos_fallos
	end

	def incrementar_intentos_logueo_fallidos
		@intentos_logueo_fallidos = @intentos_logueo_fallidos + 1
	end

	def es_password_correcto?(password)
		@password == password
	end

	def inicializar_intentos_fallidos
		@intentos_logueo_fallidos = 0
	end
end