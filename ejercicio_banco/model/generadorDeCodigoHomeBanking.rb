class GeneradorDeCodigoHomeBanking
	
	def initialize
		@codigo = 10000
	end

	def generar_codigo_home_banking(cliente)
		@codigo = @codigo + 1

		cliente.codigo_home_banking = @codigo

		return @codigo
	end

end