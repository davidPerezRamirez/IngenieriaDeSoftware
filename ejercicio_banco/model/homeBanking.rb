
require_relative './repositorioDeUsuarios'

class HomeBanking	

	MAXIMO_INTENTOS_FALLIDOS = 3 unless const_defined?(:MAXIMO_INTENTOS_FALLIDOS)	

	def initialize(banco)				
		@banco = banco
		@repositorio_usuarios = RepositorioDeUsuarios.new		
	end	

	def loguear_usuario(email, password)			
		usuario = @banco.obtener_cliente(email)
		
		if (esta_registrado_cliente?(email) && (!usuario.es_password_correcto?(password))) then
			usuario.incrementar_intentos_logueo_fallidos
		end

		acceso = esta_registrado_cliente?(email) && 
				usuario.es_password_correcto?(password) && 
				(!usuario.tiene_credenciales_bloquedas?(MAXIMO_INTENTOS_FALLIDOS))

		if (acceso) then
			usuario.inicializar_intentos_fallidos
		end

		return acceso
	end

	def registrar_usuario(email, password, codigo)		
		cliente = @banco.obtener_cliente(email)

		if ((!esta_registrado_cliente?(email)) &&
			es_valido_codigo?(cliente, codigo)) then	
					
			usuario = @repositorio_usuarios.agregar_usuario(cliente);		
		end

		return usuario
	end

	def esta_registrado_cliente?(email) 
		@repositorio_usuarios.existe_usuario?(email)
	end

	def es_valido_codigo?(usuario, codigo)
		(!usuario.nil?) && usuario.codigo_home_banking.eql?(codigo)
	end

	def cantidad_usuarios_registrados
		@repositorio_usuarios.cantidad_usuarios_registrados
	end

end