require_relative './clienteBanco'

class RepositorioDeUsuarios
	
	def initialize
		@usuarios = []
	end

	def agregar_usuario(usuario)		
		@usuarios << usuario
	end

	def obtener_usuario(email)
		@usuarios.detect { 
			|usuario|
			usuario.email.eql?(email)
		}
	end	

	def existe_usuario?(email)
		@usuarios.any? { 
			|usuario|
			usuario.email.eql?(email)
		}
	end

	def cantidad_usuarios_registrados
		@usuarios.size
	end

end