class ValidadorPassword
	
	TAMANIO_PASSWORD = 8 unless const_defined?(:TAMANIO_PASSWORD)

	def es_password_valido?(password)
		password.size.eql?(TAMANIO_PASSWORD) &&
		(password =~ /[A-Z]/).eql?(0)
	end
	
end