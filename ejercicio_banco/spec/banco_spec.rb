require 'rspec' 
require_relative '../model/banco'

describe 'Banco' do

  subject(:banco) {Banco.new}

  it 'validar no se agrega cliente con password con tamanio <> 8' do          
    banco.agregar_cliente("cliente", "R112354353645")

    expect(banco.cantidad_usuarios_registrados).to eq 0
  end

  it 'se agrega cliente con password tamanio == 8' do          
    banco.agregar_cliente("cliente", "R1234567")

    expect(banco.cantidad_usuarios_registrados).to eq 1
  end 

end