require 'rspec' 
require_relative '../model/homeBanking'
require_relative '../model/banco'

describe 'homeBanking' do

  let(:banco) {Banco.new}
  subject(:home_banking) { home = HomeBanking.new(banco) }
  let(:codigo_home_banking) {
    cliente = banco.agregar_cliente("email@gmail.com", "R1234567")
    banco.generar_codigo_home_banking(cliente)
  }
   
  it 'registrar usuario inexistente deberia agregar un usuario a lista de usuarios del home_banking' do
  	cant_inicial = home_banking.cantidad_usuarios_registrados
  	home_banking.registrar_usuario("email@gmail.com", "R1234567", codigo_home_banking)

    expect(home_banking.cantidad_usuarios_registrados).to eq cant_inicial + 1
  end

  it 'registrar un usuario existente no deberia agregar usuario a lista de usuarios home_banking' do  	
  	home_banking.registrar_usuario("email@gmail.com", "R1234567", codigo_home_banking)
  	cant_inicial = home_banking.cantidad_usuarios_registrados  	
  	home_banking.registrar_usuario("email@gmail.com", "R1234567", codigo_home_banking)

    expect(home_banking.cantidad_usuarios_registrados).to eq cant_inicial
  end

  it 'loguear usuario existente deberia habilitar acceso' do
    home_banking.registrar_usuario("email@gmail.com", "R1234567", codigo_home_banking)   	  	

    expect(home_banking.loguear_usuario("email@gmail.com", "R1234567")).to eq true
  end

  it 'loguear usuario inexistente deberia rechazar acceso' do  	  	  	  
    expect(home_banking.loguear_usuario("email@gmail.com", "R1234567")).to eq false
  end


end