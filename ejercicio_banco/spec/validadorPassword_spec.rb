require 'rspec' 
require_relative '../model/validadorPassword'

describe 'ValidadorPassword' do

  subject(:validadorPassword) {ValidadorPassword.new}    

  it 'validar password de cliente deberia devolver false para password de tamanio <> 8' do    
    expect(validadorPassword.es_password_valido?("R123")).to eq false
  end

  it 'password debe contener al menos un caracter mayuscula deberia devolver true para password R1234567' do              
    expect(validadorPassword.es_password_valido?("R1234567")).to eq true
  end

  it 'password debe contener al menos un caracter mayuscula deberia devolver false para password 12345678' do              
    expect(validadorPassword.es_password_valido?("12345678")).to eq false
  end

end