Consideraciones de diseño:
	
	Clase OrdenDeCompra: Este clase recibe el precioUnitario y cantidad de items.
		- Si cantidad de items y/o precio unitario tienen un valor alfanumerico solo se consideraran los numeros. 
		- Si cantidad de items y/o precio unitario solo poseen letras o no tienen nada se tomara como ingreso 0.0.
		- Si no hay ningun codigo de provincia asigando a la orden de compra o para codigo asignado no hay un impuesto a aplicar no se aplicara ningun impuesto al monto a pagar.
		- El calculo del monto a pagar siempre devuelve el valor absoluto. Es decir, si la cantidad de items o precio unitario es negativo se tomara el valor absoluto del resultado que en este caso seria negativo.


	Clase ProgramFacturador: Tiene por objetivo brindar una interfaz para el ingreso de datos del usuario por consola.

		- Para ejecutarla se debe desde consola: 
			1. posicionarse sobre la carpeta /ejercicio_elefante_carpaccio
			2. ingresar comando: irb
			3. ingresar comando: load "programaFacturador.rb"
			4. se crea la varible facturador: facturador = ProgramaFacturador.new
			5. Se inicia el ingreso de datos: facturador.iniciar
		- Si el usuario ingresa en cantidad de items y/o precio unitario un valor alfanumerico o un valor negativo se lanzara una exception informando que el valor ingresado no es valido.


