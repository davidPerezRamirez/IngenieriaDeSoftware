Feature: Calculo de monto de orden

Background:
	Given se genera un orden de compra 

Scenario Outline: Calculo para monto a pagar sin impuesto provincia
  Given se compran "<cantidadItems>" items
  And el precio unitario por item es $"<precioUnitario>"
  And la compra se realiza en "<provincia>"      
  When se calcula el monto a pagar
  Then se debe obtener $"<resultado>"
  Examples:
  | cantidadItems | precioUnitario | provincia | resultado |
  |  	 32		      | 	20 		       |		       | 	640      |
  |  	 100	      | 	10 		       |	  	     | 	970      |
  |  	 350	      | 	10 		       |	  	     | 	3395     |
  |  	 500	      | 	10   	       |		       | 	4750     |
  |  	 420	      | 	20   	       |	TF 	     | 	7812     |
  |  	 700	      | 	10   	       |	ET       | 	6510     |
  |  	 560	      | 	15   	       |		       | 	7812     |
  |    1000       |   10           |           |  9000     |
  |    750        |   34.9         |           |  23557.5  |
  |    5000       |   10           |           |  42500    |      

Scenario Outline: Calculo para monto a pagar con impuesto prov. Bs.As
  Given se compran "<cantidadItems>" items
  And el precio unitario por item es $"<precioUnitario>"
  And la compra se realiza en "BA"      
  When se calcula el monto a pagar
  Then se debe obtener $"<resultado>"
  Examples:
  | cantidadItems | precioUnitario | resultado |
  |    32         |   20           |  665.6    |
  |    100        |   10           |  1008.8   |
  |    350        |   10           |  3530.8   |
  |    500        |   10           |  4940     |
  |    420        |   20           |  8124.48  |
  |    700        |   10           |  6770.4   |
  |    560        |   15           |  8124.48  |
  |    1000       |   10           |  9360     |
  |    750        |   34.9         |  24499.8  |
  |    5000       |   10           |  44200    |

Scenario Outline: Calculo para monto a pagar con impuesto prov. Santa fe
  Given se compran "<cantidadItems>" items
  And el precio unitario por item es $"<precioUnitario>"
  And la compra se realiza en "SF"      
  When se calcula el monto a pagar
  Then se debe obtener $"<resultado>"
  Examples:
  | cantidadItems | precioUnitario | resultado |
  |    32         |   20           |  683.84   |
  |    100        |   10           |  1036.445 |
  |    350        |   10           |  3627.5575|
  |    500        |   10           |  5075.375 |
  |    420        |   20           |  8347.122 |
  |    700        |   10           |  6955.935 |
  |    560        |   15           |  8347.122 |
  |    1000       |   10           |  9616.5   |
  |    750        |   34.9         |  25171.18875 |
  |    5000       |   10           |  45411.25 |

Scenario Outline: Calculo para monto a pagar con impuesto prov. Cordoba
  Given se compran "<cantidadItems>" items
  And el precio unitario por item es $"<precioUnitario>"
  And la compra se realiza en "CO"      
  When se calcula el monto a pagar
  Then se debe obtener $"<resultado>"
  Examples:
  | cantidadItems | precioUnitario | resultado |
  |    32         |   20           |  691.2    |
  |    100        |   10           |  1047.6   |
  |    350        |   10           |  3666.6   |
  |    500        |   10           |  5130     |
  |    420        |   20           |  8436.96  |
  |    700        |   10           |  7030.8   |
  |    560        |   15           |  8436.96  |
  |    1000       |   10           |  9720     |
  |    750        |   34.9         |  25442.1  |
  |    5000       |   10           |  45900    |

Scenario Outline: Calculo para monto a pagar con impuesto prov. Mendoza
  Given se compran "<cantidadItems>" items
  And el precio unitario por item es $"<precioUnitario>"
  And la compra se realiza en "ME"      
  When se calcula el monto a pagar
  Then se debe obtener $"<resultado>"
  Examples:
  | cantidadItems | precioUnitario | resultado |
  |    32         |   20           |  692.8    |
  |    100        |   10           |  1050.025 |
  |    350        |   10           |  3675.0875|
  |    500        |   10           |  5141.875 |
  |    420        |   20           |  8456.49  |
  |    700        |   10           |  7047.075 |
  |    560        |   15           |  8456.49  |
  |    1000       |   10           |  9742.5   |
  |    750        |   34.9         |  25500.99375  |
  |    5000       |   10           |  46006.25  |
