Feature: Ingreso de datos en facturador

Background:
	Given se inicia el facturador de orden

Scenario: Se genera error al ingresar cantidad de items negativa
  Given se ingresan "-10" items a comprar     
  Then se advierte que la cantidad de items ingresados debe ser positiva

 Scenario: Se genera error al ingresar cantidad de items alfanumerica
  Given se ingresan "12asd" items a comprar     
  Then se advierte que las cantidad de items ingresados deber ser numerico

Scenario: Se genera error al ingresar precio unitario negativo
  Given se ingresa precio unitario $"-25"    
  Then se ingresa advierte que el precio unitario ingresado debe ser positivo

Scenario: Se genera error al ingresar como precio unitario negativo un alfanumerico
  Given se ingresa precio unitario $"25pesos"  
  Then se advierte que el precio unitario ingresado deber ser numerico

 Scenario: Se genera error al no ingresar la cantidad de items
  Given no se ingresa la cantidad de items 
  And se ingresa precio unitario $"85"  
  Then se advierte que las cantidad de items ingresados deber ser numerico

 Scenario: Se genera error al no ingresar el precio unitario
  Given se ingresan "25" items a comprar  
  And no se ingresa el precio unitario por item
  Then se advierte que el precio unitario ingresado deber ser numerico