Given(/^se genera un orden de compra$/) do
  @orden = OrdenDeCompra.new
end

Given(/^se compran "([^"]*)" items$/) do |cant_items|
  @orden.items = cant_items.to_i
end

Given(/^el precio unitario por item es \$"([^"]*)"$/) do |precio|
  @orden.precio_unitario = precio.to_f
end

When(/^se calcula el monto a pagar$/) do  
  @monto_pagar = @orden.calcular_monto_a_pagar
end

Then(/^se debe obtener \$"([^"]*)"$/) do |total|
  expect(@monto_pagar).to eq total.to_f
end

Given(/^la compra se realiza en "([^"]*)"$/) do |prov|
  @orden.provincia = prov
end


