Given(/^se inicia el facturador de orden$/) do
  @facturador = ProgramaFacturador.new
end

Given(/^se ingresan "([^"]*)" items a comprar$/) do |cant_items|
  @cantidad_items = cant_items
end

Then(/^se advierte que la cantidad de items ingresados debe ser positiva$/) do
  expect { @facturador.guardar_cantidad_de_items(@cantidad_items) }.to raise_error(RangeError)
end

Then(/^se advierte que las cantidad de items ingresados deber ser numerico$/) do
  expect { @facturador.guardar_cantidad_de_items(@cantidad_items) }.to raise_error(TypeError)
end

Then(/^se ingresa advierte que el precio unitario ingresado debe ser positivo$/) do
  expect { @facturador.guardar_cantidad_de_items(@precio_unitario) }.to raise_error(RangeError)
end

Then(/^se advierte que el precio unitario ingresado deber ser numerico$/) do
  expect { @facturador.guardar_cantidad_de_items(@precio_unitario) }.to raise_error(TypeError)
end

Given(/^se ingresa precio unitario \$"([^"]*)"$/) do |precio|
  @precio_unitario = precio
end

Given(/^no se ingresa la cantidad de items$/) do
  @cantidad_items = ""
end

Given(/^no se ingresa el precio unitario por item$/) do
  @precio_unitario = ""
end


