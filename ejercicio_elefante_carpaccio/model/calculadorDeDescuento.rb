class CalculadorDeDescuento

	attr_reader :monto_menor
	attr_reader	:monto_mayor
	attr_reader :porcentaje_descuento	
	attr_accessor :siguiente	

	def initialize(menor, mayor = nil, porcentaje)
		@monto_mayor = mayor
		@monto_menor = menor
		@porcentaje_descuento = porcentaje
	end

	def calcular_descuento(monto) 	
		descuento = 0.0	

		if (monto >= @monto_menor && (@monto_mayor.nil? || monto < @monto_mayor)) then
			descuento = monto * @porcentaje_descuento
		elsif (!@siguiente.nil?) then				
			descuento = @siguiente.calcular_descuento(monto)
		end

		return (descuento).round(2)

	end

end