require_relative 'calculadorDeDescuento.rb'

class CalculadorDeDescuentoFactory

	def initialize
		initicializar_calculadores_descuentos
		definir_cadena_de_responsabilidad_descuentos
		
	end

	def obtener_calculador_descuento
		@descuento_monto_1000_5000
	end

	private

	def initicializar_calculadores_descuentos
		@descuento_monto_1000_5000 = CalculadorDeDescuento.new(1000,5000, 0.03)		
		@descuento_monto_5000_7000 = CalculadorDeDescuento.new(5000,7000, 0.05)
		@descuento_monto_7000_10000 = CalculadorDeDescuento.new(7000,10000, 0.07)
		@descuento_monto_10000_50000 = CalculadorDeDescuento.new(10000,50000, 0.10)
		@descuento_monto_mayor_50000 = CalculadorDeDescuento.new(50000, 0.15)
	end

	def definir_cadena_de_responsabilidad_descuentos
		@descuento_monto_1000_5000.siguiente = @descuento_monto_5000_7000
		@descuento_monto_5000_7000.siguiente = @descuento_monto_7000_10000
		@descuento_monto_7000_10000.siguiente = @descuento_monto_10000_50000		
		@descuento_monto_10000_50000.siguiente = @descuento_monto_mayor_50000
	end

end