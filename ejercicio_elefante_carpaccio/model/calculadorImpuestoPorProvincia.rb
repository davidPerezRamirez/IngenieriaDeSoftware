class CalculadorImpuestoPorProvincia

	def initialize 
		@impuestos_por_provincia = Hash.new
		cargar_impuestos_por_provincia
		
	end

	def cargar_impuestos_por_provincia		
		@impuestos_por_provincia["BA"] = 0.04
		@impuestos_por_provincia["SF"] = 0.0685
		@impuestos_por_provincia["CO"] = 0.08
		@impuestos_por_provincia["ME"] = 0.0825		
	end

	def calcular_impuesto(montoBase, provincia)
		impuesto = 0.0

		if (@impuestos_por_provincia.has_key?(provincia))then
			impuesto = montoBase * @impuestos_por_provincia[provincia] 
		end

		return impuesto
	end

end