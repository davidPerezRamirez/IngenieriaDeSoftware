require_relative "calculadorImpuestoPorProvincia"
require_relative "calculadorDeDescuentoFactory"

class OrdenDeCompra

	attr_accessor :items
	attr_accessor :precio_unitario
	attr_accessor :provincia

	def initialize
		@calculador_impuesto_por_provincia = CalculadorImpuestoPorProvincia.new
		@calculador_de_descuento_factory = CalculadorDeDescuentoFactory.new
	end

	def calcular_monto_sin_descuento_ni_impuesto
		@items * @precio_unitario
	end

	def calcular_monto_sin_descuento_con_impuesto(monto_base)		
		impuesto = @calculador_impuesto_por_provincia.calcular_impuesto(monto_base, @provincia)

		return monto_base + impuesto
	end

	def calcular_monto_con_descuento_sin_impuesto(monto_base)		
		descuento = obtener_calculador_descuento.calcular_descuento(monto_base)

		return monto_base - descuento
	end

	def calcular_monto_a_pagar
		monto_base = calcular_monto_sin_descuento_ni_impuesto
		monto_con_descuento = calcular_monto_con_descuento_sin_impuesto(monto_base)
		monto_total = calcular_monto_sin_descuento_con_impuesto(monto_con_descuento)

		return monto_total.abs
	end

	private

	def obtener_calculador_descuento
		@calculador_de_descuento_factory.obtener_calculador_descuento
	end

end