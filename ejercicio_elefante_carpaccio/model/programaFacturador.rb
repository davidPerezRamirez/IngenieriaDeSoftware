require_relative 'ordenDeCompra'
require_relative "verificadorNumerico"

class ProgramaFacturador

	def initialize
		@orden = OrdenDeCompra.new
		@verificador_numerico = VerificadorNumerico.new
	end

	def iniciar			
		@continuar = true
		while (@continuar) do 
			mostrar_titulo
			ingresar_cantidad_de_items			
			ingresar_precio_unitario			
			ingresar_provincia			
			mostrar_precio_total			
			continuar_ingresando_datos?			
		end
	end

	def guardar_cantidad_de_items(cant_items)
		@verificador_numerico.validar_valor_numerico_positivo(cant_items)
		@orden.items = cant_items.to_f
	end

	private

	def ingresar_cantidad_de_items
		puts "Ingrese la cantidad de Items a comprar:"  
		STDOUT.flush  
		cant_items = gets.chomp
		guardar_cantidad_de_items(cant_items)
		puts "\n"				
	end

	def ingresar_precio_unitario
		puts "Ingrese precio unitario por item:"  
		STDOUT.flush  
		precio = gets.chomp
		guardar_precio_unitario(precio)
		puts "\n"
	end

	def guardar_precio_unitario(precio)
		@verificador_numerico.validar_valor_numerico_positivo(precio)
		@orden.precioUnitario = precio.to_f
	end

	def ingresar_provincia
		puts "Ingrese el codigo provincia donde se realiza la compra:"  
		STDOUT.flush  
		@orden.provincia = gets.chomp
		puts "\n"
	end

	def mostrar_precio_total
		total = @orden.calcular_monto_a_pagar
		puts "Total a pagar: " + total.to_s 
		puts "\n"				
	end

	def continuar_ingresando_datos?
		error_de_ingreso = true

		while (error_de_ingreso) do
			puts "¿Desea realizar otra compra? (y/n) "  
			STDOUT.flush 
			respuesta = gets.chomp
			@continuar = (respuesta.upcase.eql?"Y")
			error_de_ingreso = (not @continuar) && (not (respuesta.upcase.eql?"N"))
			puts "\n"
		end
	end

	def mostrar_titulo
		puts "\n****************************\n"
		puts "       Orden de compra		  "
		puts "******************************\n"
	end

end