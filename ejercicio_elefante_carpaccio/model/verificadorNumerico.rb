class VerificadorNumerico

	def validar_valor_numerico_positivo (cant_items)
		if (not es_numero?(cant_items)) then
			raise TypeError, "El valor ingresado debe ser numerico"			
		elsif (not es_numero_positivo?(cant_items)) then
			raise RangeError, "El valor ingresado debe ser mayor a cero"			
		end		
	end	

	def es_numero?(valor)
		true if Float(valor) rescue false
	end

	def es_numero_positivo?(valor)
		valor.to_f >= 0.0
	end

end