require 'rspec' 
require_relative '../model/calculadorDeDescuentoFactory'

describe 'Calculador Descuentos montos entre 1000 y 5000' do

  let(:calculador) { 
    factory = CalculadorDeDescuentoFactory.new
    factory.obtener_calculador_descuento
  }
   
  it 'calcular descuento para monto entre 1000 y 5000 deberia devolver 3% monto total' do
    expect(calculador.calcular_descuento(3200.15)).to eq 96.00
  end 

  it 'calcular descuento para monto $1000 deberia devolver $30' do
    expect(calculador.calcular_descuento(1000)).to eq 30
  end 

  it 'calcular descuento para monto $5000 no deberia devolver $150' do
    expect(calculador.calcular_descuento(5000)).not_to eq 150
  end

end