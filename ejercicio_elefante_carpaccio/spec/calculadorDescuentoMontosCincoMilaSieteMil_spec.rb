require 'rspec' 
require_relative '../model/calculadorDeDescuentoFactory'

describe 'Calculador Descuentos montos entre 5000 y 7000' do

  let(:calculador) { 
    factory = CalculadorDeDescuentoFactory.new
    factory.obtener_calculador_descuento
  }  
   
  it 'calcular descuento para monto entre 5000 y 7000 deberia devolver 5% monto total' do
    expect(calculador.calcular_descuento(6321.15)).to eq 316.06
  end 

  it 'calcular descuento para monto $5000 deberia devolver $250' do
    expect(calculador.calcular_descuento(5000)).to eq 250
  end 

  it 'calcular descuento para monto $7000 no deberia devolver $350' do
    expect(calculador.calcular_descuento(7000)).not_to eq 350
  end

end