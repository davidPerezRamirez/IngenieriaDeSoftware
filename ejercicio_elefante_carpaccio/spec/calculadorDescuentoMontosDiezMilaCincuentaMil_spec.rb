require 'rspec' 
require_relative '../model/calculadorDeDescuentoFactory'

describe 'Calculador Descuentos montos entre 10000 y 50000' do

  let(:calculador) { 
    factory = CalculadorDeDescuentoFactory.new
    factory.obtener_calculador_descuento
  }
   
  it 'calcular descuento para monto entre 10000 y 50000 deberia devolver 10% monto total' do
    expect(calculador.calcular_descuento(45362)).to eq 4536.2
  end 

  it 'calcular descuento para monto 10000 deberia devolver 1000' do
    expect(calculador.calcular_descuento(10000)).to eq 1000
  end 

  it 'calcular descuento para monto 50000 no deberia devolver 5000' do
    expect(calculador.calcular_descuento(50000)).not_to eq 5000
  end

end