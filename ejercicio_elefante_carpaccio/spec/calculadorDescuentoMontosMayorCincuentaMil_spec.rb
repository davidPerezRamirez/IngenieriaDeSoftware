require 'rspec' 
require_relative '../model/calculadorDeDescuentoFactory'

describe 'Calculador Descuentos montos mayores a 50000' do

  let(:calculador) { 
    factory = CalculadorDeDescuentoFactory.new
    factory.obtener_calculador_descuento
  }
   
  it 'calcular descuento para monto mayor a 50000 deberia devolver 15% monto total' do
    expect(calculador.calcular_descuento(120000)).to eq 18000
  end 

  it 'calcular descuento para de 50000 deberia devolver 15% monto total' do
    expect(calculador.calcular_descuento(50000)).to eq 7500
  end 

end