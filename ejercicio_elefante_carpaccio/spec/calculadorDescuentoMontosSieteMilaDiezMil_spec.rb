require 'rspec' 
require_relative '../model/calculadorDeDescuentoFactory'

describe 'Calculador Descuentos montos entre 7000 y 10000' do

  let(:calculador) { 
    factory = CalculadorDeDescuentoFactory.new
    factory.obtener_calculador_descuento
  }
   
  it 'calcular descuento para monto entre 7000 y 10000 deberia devolver 7% monto total' do
    expect(calculador.calcular_descuento(8352.30)).to eq 584.66
  end 

  it 'calcular descuento para monto $7000 deberia devolver $490' do
    expect(calculador.calcular_descuento(7000)).to eq 490
  end 

  it 'calcular descuento para monto $10000 no deberia devolver $700' do
    expect(calculador.calcular_descuento(10000)).not_to eq 700
  end

end