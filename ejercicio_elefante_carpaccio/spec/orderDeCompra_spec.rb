require 'rspec' 
require_relative '../model/ordenDeCompra'

describe 'Order de Compra' do

  let(:orden) { OrdenDeCompra.new }  
   
  it 'calcular monto sin descuento ni impuestos de 4 items a $20 cada uno deberia devolver $80' do
    orden.items = 4
    orden.precio_unitario = 20

    expect(orden.calcular_monto_sin_descuento_ni_impuesto).to eq 80
  end 

  it 'calcular monto sin descuento con impuesto 4% de 32 items a $20 cada uno deberia devolver $665.6' do
    orden.items = 32
    orden.precio_unitario = 20
    orden.provincia = "BA"
    montoBase = orden.calcular_monto_sin_descuento_ni_impuesto

    expect(orden.calcular_monto_sin_descuento_con_impuesto(montoBase)).to eq 665.6
  end 

  it 'calcular monto con descuento con impuesto 4% de 145 items a $25 cada uno deberia devolver $3656.9' do
    orden.items = 145
    orden.precio_unitario = 25
    orden.provincia = "BA"

    expect(orden.calcular_monto_a_pagar).to eq 3656.9
  end

end