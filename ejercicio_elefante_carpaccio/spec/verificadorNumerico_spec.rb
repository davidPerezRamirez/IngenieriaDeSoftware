require 'rspec' 
require_relative '../model/verificadorNumerico'

describe 'Order de Compra' do

  let(:verificador) { VerificadorNumerico.new }  
   
  it 'es_numero? recibe numero > 0 deberia devolver true' do    
    expect(verificador.es_numero?(123)).to eq true
  end

  it 'es_numero? recibe string numerico > 0 deberia devolver true' do    
    expect(verificador.es_numero?("487")).to eq true
  end

  it 'es_numero? recibe alfanumerico deberia devolver false' do    
    expect(verificador.es_numero?("-asd342")).to eq false
  end

  it 'es_numero_positivo? recibe numero > 0 deberia devolver true' do    
    expect(verificador.es_numero_positivo?(125)).to eq true
  end  

  it 'es_numero_positivo? recibe string numerico > 0 deberia devolver true' do    
    expect(verificador.es_numero_positivo?("12")).to eq true
  end

  it 'es_numero_positivo? recibe numero < 0 deberia devolver false' do    
    expect(verificador.es_numero_positivo?(-100)).to eq false
  end 

  it 'es_numero_positivo? recibe string numerivo < 0 deberia devolver false' do    
    expect(verificador.es_numero_positivo?("-1000.485")).to eq false
  end 

  it 'es_numero_positivo? recibe 0 deberia devolver true' do    
    expect(verificador.es_numero_positivo?(0)).to eq true
  end 
 
  it 'validar_valor_numerico_positivo recibe numero < 0 deberia devolver RangeError' do    
    expect { verificador.validar_valor_numerico_positivo(-123.45) }.to raise_error(RangeError)
  end

  it 'validar_valor_numerico_positivo recibe numero alfanumerico deberia devolver TypeError' do    
    expect { verificador.validar_valor_numerico_positivo("a123.45") }.to raise_error(TypeError)
  end 

end